

### Backup command
##### Linux
code --list-extensions | xargs -L 1 echo code --install-extension > vs_code_backup.sh
#### WIndows 
code --list-extensions | % { "code --install-extension $_" } > vs_code_backup.ps1


### Restore command
##### Linux
sh vs_code_backup.sh
#### WIndows
Set-ExecutionPolicy RemoteSigned

& "C:\vs_code_backup.ps1" 